import { writable } from 'svelte/store';

export const rows = writable(2);
export const columns = writable(2);

export const score = writable(0);
export const isGameOver = writable(false);
