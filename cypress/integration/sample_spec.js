/// <reference types="Cypress" />

// describe('My First Test', function() {
//     it('Visits the Kitchen Sink', function() {
//       cy.visit('https://example.cypress.io')

//       cy.contains('type').click()

//        // Should be on a new URL which includes '/commands/actions'
//     cy.url().should('include', '/commands/actions')
//     })
//   })


describe('Svelte memory game', function() {
    it('Checks app renders 2x2 grid', function() {
        let cards = []
        cy.visit('http://localhost:5000')
        cy.contains('Memory game')
        cy.get('.card').should('have.length', 4)
        // cy.get('.card').first().click()
    })
    
    it('plays for a bit...', () => {
        cy.get('.card')
        .then(($cards) => { console.log('Number: ', $cards.length)})
        .each(($card, i, $cards) => {
            cy.wrap($card).click()
            cy.wait(2000)
        })
    })

    it('expects to win!!!', () => {
        cy.contains('Keep Playing!')
    })
})